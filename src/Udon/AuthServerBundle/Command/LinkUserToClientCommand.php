<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 24/03/15
 * Time: 9:10 PM
 */

namespace Udon\AuthServerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Udon\AuthServerBundle\Entity\Client;
use Udon\AuthServerBundle\Entity\User;
use FOS\OAuthServerBundle\Event\OAuthEvent;
use Symfony\Component\HttpFoundation\Request;


class LinkUserToClientCommand extends ContainerAwareCommand{

    protected function configure()
    {
        $this
            ->setName('udon:auth-server:client:user:link')
            ->setDescription('Links a user with a client')
            ->addArgument('userId', InputArgument::REQUIRED, 'Sets the id of the user', null)
            ->addArgument('clientId', InputArgument::REQUIRED, 'Sets the id of the user', null)
            ->addOption('scope', null, InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'Sets scopes for the user access.', null)
            ->setHelp("This is the help");

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $clientManager = $this->getContainer()->get('fos_oauth_server.client_manager.default');
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
         
        $clientId = $input->getArgument('clientId');
        $userId = $input->getArgument('userId');

        $scopes = $input->getOption('scope');
        $scope = implode(' ', $scopes);
        
        $client = $clientManager->findClientByPublicId($clientId);
        $user = $em->getRepository('UdonAuthServerBundle:User')->find($userId);

        $user->setRoles($scopes);

        $em->persist($user);
        $em->flush();
        
        $requestQuery = [
            'client_id' => $clientId, 
            'grant_type' => 'client_credentials', 
            'redirect_uri' => $client->getRedirectUris()[0], 
            'response_type' => 'code'
        ];
        
        $request = new Request($requestQuery);

        $server = $this->getContainer()->get('fos_oauth_server.server');
        
        $result = $server->finishClientAuthorization(true, $user, $request,$scope);

        print_r($result);

    }
}