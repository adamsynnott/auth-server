<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 24/03/15
 * Time: 10:12 PM
 */

namespace Udon\AuthServerBundle\Form\Type;


use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

class AuthorizeFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options = array())
    {
        $builder->add('allowAccess', 'checkbox', array(
            'label' => 'Allow access',
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array('data_class' => 'Udon\AuthServerBundle\Form\Model\Authorize');
    }

    public function getName()
    {
        return 'udon_authserver_authorize';
    }

}