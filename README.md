OAuth 2 Api Server
========================


What's it all about?
--------------

This is written in Symfony and uses the following main packages:

  * FOS OAuth Bundle;

  * FOS User Bundle;

  * FOS REST Bundle;

Command Line Reference

Create a new client
php app/console udon:auth-server:client:create --redirect-uri=http://auth-server.local --grant-type=authorization_code adam

Create a new User
php app/console fos:user:create testuser test@example.com p@ssword

link a user to a client
php app/console udon:auth-server:client:user:link 1 1_563bnw4wgekocswko48wwss0okk4gsk0c0kk88csg8ckw448sk --scope=adam

Link a user to a client

Refer to POSTMAN Collection in the repo for the API calls


http://auth-server.local:8080/oauth/v2/token?client_id=3_1q97qwx18vhcc88kgsw0kwog4804g4k48w0c0kos8swwk4ok8s&grant_type=client_credentials&client_secret=4qigpn6ebsao4s4kggkowocgosswwc0g84wk80k4wswswsccss


Enjoy!

[1]:  auth-server.local
[2]: https://github.com/FriendsOfSymfony/FOSUserBundle/blob/master/Resources/doc/command_line_tools.md
