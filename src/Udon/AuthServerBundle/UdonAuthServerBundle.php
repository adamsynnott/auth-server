<?php

namespace Udon\AuthServerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class UdonAuthServerBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSOAuthServerBundle';
    }
}
