<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 24/03/15
 * Time: 10:29 PM
 */

namespace Udon\AuthServerBundle\Controller;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use FOS\OAuthServerBundle\Controller\AuthorizeController as BaseAuthorizeController;
use Udon\AuthServerBundle\Form\Model\Authorize;
use Udon\AuthServerBundle\Entity\Client;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class AuthorizeController extends BaseAuthorizeController
{

    public function authorizeAction(Request $request)
    {
        if (!$request->get('client_id')) {
            throw new NotFoundHttpException("Client id parameter {$request->get('client_id')} is missing.");
        }

        $clientManager = $this->container->get('fos_oauth_server.client_manager.default');
        $client = $clientManager->findClientByPublicId($request->get('client_id'));

        if (!($client instanceof Client)) {
            throw new NotFoundHttpException("Client {$request->get('client_id')} is not found.");
        }

        $user = $this->container->get('security.context')->getToken()->getUser();

        $form = $this->container->get('udon_authserver.authorize.form');
        $formHandler = $this->container->get('udon_authserver.authorize.form_handler');

        $authorize = new Authorize();

        if (($response = $formHandler->process($authorize)) !== false) {
            return $response;
        }

        return $this->container->get('templating')->renderResponse('UdonAuthServerBundle:Authorize:authorize.twig.html', array(
            'form' => $form->createView(),
            'client' => $client,
            'client_id' => $request->get('client_id'),
            'response_type' => $request->get('response_type'),
            'redirect_uri' => $request->get('redirect_uri'),
            'state' => $request->get('state'),
            'scope' => $request->get('scope'),
            'form' => $form->createView(),
        ));
    }
}