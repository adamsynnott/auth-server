<?php

namespace Udon\AuthServerBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ExclusionPolicy("all")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    protected $id;

    /**
     * @ORM\Column(type="boolean", name="deleted", options={"default": false})
     */
    protected $deleted;

    /**
     * @ORM\ManyToMany(targetEntity="Client", mappedBy="users")
     */
    protected $clients;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get id
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * @param $clients
     */
    public function setClients($clients)
    {
        $this->clients = $clients;
    }

    /**
     *
     */
    public function setDeleted()
    {
        $this->deleted = 1;
    }

    public function isDeleted()
    {
        return $this->deleted;
    }


}
