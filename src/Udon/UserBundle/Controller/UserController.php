<?php
/**
 * Created by PhpStorm.
 * User: adamS
 * Date: 11/04/15
 * Time: 12:01 AM
 */

namespace Udon\UserBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Udon\AuthServerBundle\Entity\User;
use Udon\AuthServerBundle\Entity\Client;
use Udon\UserBundle\Form\UserType;

class UserController extends FOSRestController implements ClassResourceInterface
{

    /**
     * @Rest\View()
     * @Rest\Get("api/v1/user")
     */
    public function cgetAction()
    {
        $this->get('udon_permissions')->authenticateRequestToScope('USERS.R');
        $client = $this->get('udon_permissions')->getClient();
        $users = $client->getUsers();
        if(count($users) === 0)
        {
            throw new NotFoundHttpException();
        }
        return $users;
    }

    /**
     * @Rest\View()
     * @Rest\Get("api/v1/user/{userId}")
     */
    public function getAction($userId)
    {
        $this->get('udon_permissions')->authenticateRequestToScope('USERS.R');
        $client = $this->get('udon_permissions')->getClient();
        $entity = $this->getEntity($client->getId(), $userId);
        return $entity->entity;
    }


    /**
     * Collection delete action
     * @return View|array
     * @Rest\Delete("api/v1/user/{userId}")
     */
    public function deleteAction($userId)
    {
        $this->get('udon_permissions')->authenticateRequestToScope('USERS.W');
        $client = $this->get('udon_permissions')->getClient();
        $entity = $this->getEntity($client->getId(), $userId);
        $entity->entity->setDeleted();
        $entity->em->persist($entity->entity);
        $entity->em->flush();
        return($this->view(null, Codes::HTTP_NO_CONTENT));
    }

    /**
     * Collection post action
     * @var Request $request
     * @return View|array
     * @Rest\Post("api/v1/user/")
     */
    public function postAction(Request $request)
    {

        $entity = new User();

        $form = $this->createForm(new UserType(), $entity);


        $form->handleRequest($request);

        //return $form->getData();
        $entity->setDeleted(false);
        $entity->setEnabled(true);

        //return $form;
        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $url = $this->generateUrl('get_user', ['id' => $entity->getId()]);
            return $this->redirectView($url,Codes::HTTP_CREATED);
        }

        return ['form' => $form];
    }




    private function getEntity($clientId, $userId)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em
            ->createQuery(
                'SELECT u, c FROM UdonAuthServerBundle:User u
                 JOIN u.clients c
                 WHERE c.id = :clientId
                 AND u.id = :userId
                 AND u.deleted = 0'
            )
            ->setParameter('clientId', $clientId)
            ->setParameter('userId', $userId);
        try
        {
            $entity = $query->getSingleResult();
            return (object)['em' => $em, 'entity' => $entity];
        }
        catch (\Doctrine\ORM\NoResultException $e)
        {
            throw new NotFoundHttpException();
        }
    }

} 