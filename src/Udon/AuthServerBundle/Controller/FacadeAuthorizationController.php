<?php
/**
 * Created by PhpStorm.
 * User: adamS
 * Date: 10/04/15
 * Time: 9:41 PM
 */

namespace Udon\AuthServerBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class FacadeAuthorizationController extends FOSRestController
{


    /**
     * @Route("/oauth/v2/auth_client_authorize", name="udon_authserver_client_authorize")
     */
    public function authorizeClientUserAction(Request $request)
    {
        try
        {
            $oauthHelper = $this->get('udon_authorization');
            $accessToken = $oauthHelper->getAccessToken();
            $view = View::create();
            $view->setData($accessToken);
            $view->setFormat($oauthHelper->getResponseFormat());
            return $this->handleView($view);
        }
        catch(AccessDeniedHttpException $exception)
        {
            header('HTTP/1.0 403 Forbidden');
            exit;
        }
    }

} 