<?php
/**
 * Created by PhpStorm.
 * User: adamS
 * Date: 15/04/15
 * Time: 11:54 PM
 */

namespace Udon\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username');
        $builder->add('usernameCanonical');
        $builder->add('email');
        $builder->add('emailCanonical');
        $builder->add('enabled');
        $builder->add('salt','text',array('mapped'=>false));
        $builder->add('password');
        $builder->add('plainPassword');
        $builder->add('plainPassword');
        $builder->add('passwordRequestedAt');
        $builder->add('groups','text',array('mapped'=>false));
        $builder->add('locked');
        $builder->add('expired');
        $builder->add('roles', 'collection',array('allow_add' => true,));
        $builder->add('credentialsExpired');
        $builder->add('deleted','text',array('mapped'=>false));

    }
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Udon\AuthServerBundle\Entity\User',
                'csrf_protection' => false,
            )
        );
    }
    public function getName()
    {
        return 'user';
    }
}
