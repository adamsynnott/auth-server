<?php
/**
 * Class Permissions
 * @package Udon\AuthServerBundle\DependencyInjection
 * User: adamS
 * Date: 12/04/15
 * Time: 1:11 AM
 */
namespace Udon\AuthServerBundle\DependencyInjection;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class Permissions {

    private $request; // the current request
    private $tokenManager;
    private $securityContext;
    private $clientPrefix;
    private $scopeSeparator;
    private $permissionsSeparator;

    /**
     * @param $clientPrefix
     * @param $scopeSeparator
     * @param $permissionsSeparator
     */
    public function __construct($clientPrefix, $scopeSeparator, $permissionsSeparator)
    {
        $this->clientPrefix = $clientPrefix;
        $this->scopeSeparator = $scopeSeparator;
        $this->permissionsSeparator = $permissionsSeparator;
    }

    /**
     * @param RequestStack $requestStack
     * @param $tokenManager
     * @param $securityContext
     */
    public function init(RequestStack $requestStack, $tokenManager, $securityContext)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->tokenManager = $tokenManager;
        $this->securityContext = $securityContext;
    }

    /**
     * @param $scope
     */
    public function authenticateRequestToScope($scope)
    {
        $client = $this->getClient();
        $accessToken = $this->getAccessToken();
        $allowedScopes = $this->explodeScopes($accessToken->getScopeArray());
        $requestedScope = $this->explodeScopes(["{$this->clientPrefix}{$client->getId()}{$this->scopeSeparator}{$scope}"]);
        $this->checkAccessToScope($allowedScopes, $requestedScope);
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        $accessToken = $this->getAccessToken();
        return $accessToken->getClient();
    }

    /**
     * @return mixed
     */
    private function getAccessToken()
    {
        $token = $this->securityContext->getToken()->getToken();
        return $this->tokenManager->findTokenBy(array('token' => $token));
    }

    /**
     * @param $scopes
     * @return array
     * @throws \Exception
     */
    public function explodeScopes($scopes)
    {
        $explodedScopes = [];
        foreach($scopes as $scope)
        {
            $explodedScope = $this->explodeScope($scope);
            if(!isset($explodedScopes[$explodedScope['client']]))
            {
                $explodedScopes[$explodedScope['client']] = [];
            }
            $explodedScopes[$explodedScope['client']][$explodedScope['service']] = $explodedScope['permissions'];
        }
        return $explodedScopes;
    }

    /**
     * @param $scope
     * @return array
     * @throws \Exception
     */
    public function explodeScope($scope)
    {
        $permissions = [];
        $clientAndServiceSeparated = explode("_", $scope);
        if(count($clientAndServiceSeparated) !== 2)
        {
            throw new \Exception("Invalid Scope");
        }
        $serviceSeparatedFromPermissions = explode(".", $clientAndServiceSeparated[1]);
        if(count($clientAndServiceSeparated) !== 2)
        {
            throw new \Exception("Invalid Scope");
        }
        foreach(str_split($serviceSeparatedFromPermissions[1]) as $permission)
        {
            $permissions[] = $permission;
        }
        return [
            'client' =>$clientAndServiceSeparated[0],
            'service' => $serviceSeparatedFromPermissions[0],
            'permissions' => $permissions,
        ];
    }

    /**
     * @param $allowedScopes
     * @param $requestedScope
     */
    public function checkAccessToScope($allowedScopes, $requestedScope)
    {
        // TODO refactor this!!
        reset($requestedScope);
        $clientKey = key($requestedScope);
        $service = $requestedScope[$clientKey];
        reset($service) ;
        $permissionsKey = key($service);

        if(isset($allowedScopes[$clientKey][$permissionsKey]) && !empty($allowedScopes[$clientKey][$permissionsKey]))
        {
            foreach(($requestedScope[$clientKey][$permissionsKey]) as $permission)
            {
                if(!in_array($permission, $allowedScopes[$clientKey][$permissionsKey]))
                {
                    throw new AccessDeniedHttpException();
                }
            }
        }
        else
        {
            throw new AccessDeniedHttpException();
        }
    }

} 