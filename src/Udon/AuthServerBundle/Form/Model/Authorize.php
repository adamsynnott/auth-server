<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 24/03/15
 * Time: 10:16 PM
 */

namespace Udon\AuthServerBundle\Form\Model;

class Authorize
{
    protected $allowAccess;

    public function getAllowAccess()
    {
        return $this->allowAccess;
    }

    public function setAllowAccess($allowAccess)
    {
        $this->allowAccess = $allowAccess;
    }
}