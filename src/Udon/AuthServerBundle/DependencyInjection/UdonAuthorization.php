<?php
/**
 * Created by PhpStorm.
 * User: adamS
 * Date: 10/04/15
 * Time: 10:58 PM
 */


namespace Udon\AuthServerBundle\DependencyInjection;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class OauthHelper
 * @package Udon\AuthServerBundle\DependencyInjection
 */
class UdonAuthorization {

    private $request;
    private $clientManager;
    private $userManager;
    private $server;
    private $encoder;

    /**
     * @param RequestStack $requestStack
     * @param $clientManager
     * @param $userManager
     * @param $server
     * @param $encoder
     */
    public function init(RequestStack $requestStack, $clientManager, $userManager, $server, $encoder)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->clientManager = $clientManager;
        $this->userManager = $userManager;
        $this->server = $server;
        $this->encoder = $encoder;
    }

    /**
     * @return mixed
     * @throws AccessDeniedHttpException
     */
    public function getAccessToken()
    {
        $providedCredentials = $this->exctractUserCredentialsFromRequestHeaders();
        $user = $this->userManager->findUserByUsername($providedCredentials[0]);
        $scopes = $this->getScopesFromUser($user);
        $this->authorizeUser($user, $providedCredentials);
        $client = $this->extractClientFromRequestHeaders();
        return $this->server->createAccessToken($client, $user, $scopes, null, true, null);

    }

    /**
     * @return string
     */
    public function getResponseFormat()
    {
        $requestedResponseFormat = explode(" ",$this->request->headers->get('accept'));
        $format = 'json';
        if(!empty($requestedResponseFormat))
        {
            switch($requestedResponseFormat[0])
            {
                case 'application/xml' :
                    $format = 'xml';
                    break;
            }
        }
        return $format;
    }


    /**
     * @return array
     * @throws AccessDeniedHttpException
     */
    private function exctractUserCredentialsFromRequestHeaders()
    {
        $authorization = explode(" ",$this->request->headers->get('authorization'));

        if($authorization[0] !== 'Basic')
        {
            throw new AccessDeniedHttpException();
        }
        $rawCredentials = base64_decode($authorization[1]);
        $credentials = explode(':', $rawCredentials);
        if(count($credentials) !== 2)
        {
            throw new AccessDeniedHttpException();
        }
        return $credentials;

    }

    /**
     * @param $user
     * @param $providedCredentials
     * @throws AccessDeniedHttpException
     */
    private function authorizeUser($user, $providedCredentials)
    {
        $encoded = $this->encoder->encodePassword($user, $providedCredentials[1]);
        if($encoded != $user->getPassword())
        {
            throw new AccessDeniedHttpException();
        }
    }

    /**
     * @return mixed
     * @throws AccessDeniedHttpException
     */
    private function extractClientFromRequestHeaders()
    {
        $clientId = $this->request->get('client_id');
        $client = $this->clientManager->findClientByPublicId($clientId);
        if(!$client)
        {
            throw new AccessDeniedHttpException();
        }
        return $client;
    }

    /**
     * @param $user
     * @return string
     */
    private function getScopesFromUser($user)
    {
        $scopes = implode(' ', $user->getRoles());
        $scopes = str_replace("ROLE_USER", "", $scopes);
        return trim($scopes);
    }


} 