<?php

namespace Udon\AuthServerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Udon\AuthServerBundle\Entity\User;

class DefaultController extends Controller
{
    
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        return array('name' => "hello");
    }
    
    
    /**
     * @Route("api/client/{name}")
     * @Template()
     */
    public function clientAction(Request $request, $name)
    {
        die();

        $token = $this->container->get('security.context')->getToken()->getToken();

        echo $token;
        die();
        # this is it
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }

        //Get the current user, check if it's an admin
        $token = $this->container->get('security.context')->getToken()->getToken();

        $accessToken = $this->container
        ->get('fos_oauth_server.access_token_manager.default')
        ->findTokenBy(array('token' => $token));

        $client = $accessToken->getClient();

        $scopes = explode(" ", $accessToken->getScope());

        if(in_array('lisja', $scopes))
        {
            //echo "yo";
        }
        else
        {
           // echo "no";
        }


        return array('name' => $name);
    }

    /**
     * @Route("cljient/{name}")
     * @Template()
     */
    public function clientAuthAction($name)
    {

        $clientManager = $this->container->get('fos_oauth_server.client_manager.default');
        $client = $clientManager->createClient();
        $client->setRedirectUris(array('http://auth-server.local'));
        $client->setAllowedGrantTypes(array('token', 'authorization_code'));
        $clientManager->updateClient($client);

        return $this->redirect($this->generateUrl('fos_oauth_server_authorize', array(
            'client_id'     => $client->getPublicId(),
            'redirect_uri'  => 'http://auth-server.local',
            'response_type' => 'code'
        )));


    }



}
